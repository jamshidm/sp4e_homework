import subprocess
import pathlib
import pandas as pd
import numpy as np
import scipy.optimize
import matplotlib.pyplot as plt



# Function to read positions of a planet
def readPositions(planet_name, directory):
    directory = pathlib.Path(directory).absolute()
    positions = list()
    for file in directory.glob('step-*.csv'):
        try:
            df = pd.read_csv(file, header=None, sep=' ', usecols=[1, 2, 3, 14])
            pos = df.loc[df[14] == planet_name, :3].to_numpy()[0]
        except ValueError:
            df = pd.read_csv(file, header=None, sep=' ', usecols=[1, 2, 3, 12])
            pos = df.loc[df[12] == planet_name, :3].to_numpy()[0]
        positions.append(pos)
    return np.array(positions)



# Function to compute the error between the reference and computed positions
def computeError(positions, positions_ref):
    return np.sqrt(np.sum(np.linalg.norm(positions - positions_ref, axis=1)))



# Function to generate a new input file with scaled velocity for a given planet  
def generateInput(scale, planet_name, input_filename, output_filename):
    with open(input_filename, 'r') as f_in, open(output_filename, 'w') as f_out:
        for line in f_in:
            parts = line.split()
            if parts[-1] == planet_name:
                velocity = list(map(float, parts[3:6]))
                velocity = [v * scale for v in velocity]
                parts[3:6] = map(str, velocity)
                line = ' '.join(parts) + '\n'
            f_out.write(line)



# Function to launch the particle simulation
def launchParticles(input, nb_steps, freq):
    subprocess.run(["python3", "main.py", str(nb_steps), str(freq), input, "planet", "1"])



# Function to run the simulation with a scaled velocity and compute the error
def runAndComputeError(scale, planet_name, input, nb_steps, freq):
    output = "temp.csv"
    generateInput(scale, planet_name, input, output)
    launchParticles(output, nb_steps, freq)
    positions = readPositions(planet_name, "dumps")
    positions_ref = readPositions(planet_name, "trajectories")
    return computeError(positions, positions_ref)



# Function to optimize the initial velocity of a planet to minimize the error
def optimizeVelocity(planet_name, input, nb_steps, freq):
    errors = []
    scales = []

    def func(scale):
        error = runAndComputeError(scale[0], planet_name, input, nb_steps, freq)
        errors.append(error)
        scales.append(scale[0])
        print(f'Error: {error:.5f}, Scale: {scale[0]:.5f}')
        return error
    
    init_value = 1.0
    optimal_scale = scipy.optimize.fmin(func, init_value)

    print('\noptimal_scale: ', optimal_scale[0])

    plt.figure(figsize=(10, 5))
    plt.bar(scales, errors, width=0.03, align='center', alpha=0.7)
    plt.xlabel('Scaling Factor')
    plt.ylabel('Error')
    plt.title('Error vs Scaling Factor')
    plt.grid(True)
    plt.show()

    return optimal_scale



if __name__ == '__main__':
    optimal_scale = optimizeVelocity('mercury', 'init.csv', 365, 1)
