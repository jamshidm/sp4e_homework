#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <functional>

#include "compute_temperature.hh"
#include "csv_writer.hh"
#include "particles_factory_interface.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "compute.hh"
#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "system.hh"
#include "system_evolution.hh"

namespace py = pybind11;

PYBIND11_MODULE(pypart, m) {
  m.doc() = "pybind of the Particles project";

    // Python binding for the Compute class
    py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute")
        .def("compute", &Compute::compute);

    // Python binding for the ComputeInteraction class
    py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction");

    // Python binding for the ComputeGravity class
    py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
        .def(py::init<>())
        .def("setG", &ComputeGravity::setG);

    // Python binding for the ComputeTemperature class
    py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
        .def(py::init<>())
        .def_property("conductivity",
            &ComputeTemperature::getConductivity,
            [](ComputeTemperature& obj, Real conductivity) { obj.getConductivity() = conductivity; }
        )
        .def_property("L",
            &ComputeTemperature::getL,
            [](ComputeTemperature& obj, Real L) { obj.getL() = L; }
        )
        .def_property("capacity",
            &ComputeTemperature::getCapacity,
            [](ComputeTemperature& obj, Real capacity) { obj.getCapacity() = capacity; }
        )
        .def_property("density",
            &ComputeTemperature::getDensity,
            [](ComputeTemperature& obj, Real density) { obj.getDensity() = density; }
        )
        .def_property("deltat",
            &ComputeTemperature::getDeltat,
            [](ComputeTemperature& obj, Real deltat) { obj.getDeltat() = deltat; }
        );

    // Python binding for the ComputeVerletIntegration class
    py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
        .def(py::init<Real>())
        .def("addInteraction", &ComputeVerletIntegration::addInteraction);

    // Python binding for the ParticlesFactoryInterface class
    py::class_<ParticlesFactoryInterface, std::shared_ptr<ParticlesFactoryInterface>>(m, "ParticlesFactoryInterface")
        .def("createSimulation",
            py::overload_cast<const std::string&, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>),
            py::return_value_policy::reference
        )
        .def_static("getInstance",
            &ParticlesFactoryInterface::getInstance,
            py::return_value_policy::reference
        )
        .def_property_readonly("system_evolution",
            &ParticlesFactoryInterface::getSystemEvolution
        );

    // Python binding for the PlanetsFactory class
    py::class_<PlanetsFactory, ParticlesFactoryInterface, std::shared_ptr<PlanetsFactory>>(m, "PlanetsFactory")
        .def("createSimulation",
            py::overload_cast<const std::string&, Real>(&PlanetsFactory::createSimulation)
        )
        .def("createSimulation",
            py::overload_cast<const std::string&, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>),
            py::return_value_policy::reference
        )
        .def_static("getInstance",
            &PlanetsFactory::getInstance,
            py::return_value_policy::reference
        );

    // Python binding for the MaterialPointsFactory class
    py::class_<MaterialPointsFactory, ParticlesFactoryInterface, std::shared_ptr<MaterialPointsFactory>>(m, "MaterialPointsFactory")
        .def("createSimulation",
            py::overload_cast<const std::string&, Real>(&MaterialPointsFactory::createSimulation)
        )
        .def("createSimulation",
            py::overload_cast<const std::string&, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>),
            py::return_value_policy::reference
        )
        .def_static("getInstance",
            &MaterialPointsFactory::getInstance,
            py::return_value_policy::reference
        );

    // Python binding for the PingPongBallsFactory class
    py::class_<PingPongBallsFactory, ParticlesFactoryInterface, std::shared_ptr<PingPongBallsFactory>>(m, "PingPongBallsFactory")
        .def("createSimulation",
            py::overload_cast<const std::string&, Real>(&PingPongBallsFactory::createSimulation)
        )
        .def("createSimulation",
            py::overload_cast<const std::string&, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>),
            py::return_value_policy::reference
        )
        .def_static("getInstance",
            &PingPongBallsFactory::getInstance,
            py::return_value_policy::reference
        );

    // Python binding for the System class
    py::class_<System, std::shared_ptr<System>>(m, "System");

    // Python binding for the SystemEvolution class
    py::class_<SystemEvolution, std::shared_ptr<SystemEvolution>>(m, "SystemEvolution")
        .def("evolve", &SystemEvolution::evolve)
        .def("addCompute", &SystemEvolution::addCompute)
        .def("setNSteps", &SystemEvolution::setNSteps)
        .def("setDumpFreq", &SystemEvolution::setDumpFreq)
        .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference);

    // Python binding for the CsvWriter class
    py::class_<CsvWriter, std::shared_ptr<CsvWriter>>(m, "CsvWriter")
        .def(py::init<const std::string &>())
        .def("write", &CsvWriter::write);
    }
