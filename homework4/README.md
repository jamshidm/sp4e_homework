# SP4E Homework 4

_Mohammad Jamshidmofid & Loïc Jeanningros_

Directory for handing the fourth homework of _Scientific Programming for Engineers_ course

## Installation
First clone the repository and go to the created directory:
```commandline
git clone https://gitlab.epfl.ch/jamshidm/sp4e_homework.git
cd sp4e_homework/homework4
```

Download eigen and initialize submodules:
```commandline
git clone https://gitlab.com/libeigen/eigen.git
git submodule update --init
```

For Python use, it is always a good idea to create a virtualenv.
Come back to `homework4` directory, activate it and install requirements:

```commandline
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage

From `homework4` directory, Follow the following steps:

1. Build the project:
```commandline
mkdir build
cd build
cmake ..
make
cd src
```
2. Run the project:

The project can be run using the following command:
```commandline
python3 main.py 365 1 init.csv planet 1
```
To execute the optimization routine, run the following command:
```commandline
python3 planet.py
```

## Written Answers

### Exercise 1
The `createSimulation` method in the `ParticlesFactory` class has been overloaded to accept a functor as an argument. This allows the method to take a function or an object that implements the function call operator (operator()) as an argument. This can be useful for providing custom behavior that will be used during the simulation creation process. In the code, the functor is used to create and configure compute objects based on the type of particle being simulated.

### Exercise 2
In order to properly manage references to `Compute` objects within the Python bindings, we employ `std::shared_ptr`. This type of smart pointer automatically handles memory management, ensuring that `Compute` objects are properly deallocated when they’re no longer needed. This is crucial because the garbage collector in Python does not automatically manage memory that has been allocated in C++.

### Exercise 7
For finding the correct initial velocity for Mercury and plotting the evolution of the error versus the scaling factor, we defined the function `optimizeVelocity`. This function takes as arguments the name of a planet, an input file, the number of steps, and a frequency.

The `optimizeVelocity` function uses the `scipy.optimize.fmin` function to find the scaling factor that minimizes the error. This is achieved by defining a helper function within `optimizeVelocity` that computes the error for a given scaling factor. This helper function, in turn, uses the `runAndComputeError` function.

The `runAndComputeError` function generates an input file with the scaled velocity, runs the particle simulation, reads the resulting positions, and computes the error.

The `optimizeVelocity` function also plots the error versus the scaling factor.

