#include "RiemannIntegral.hh"

// Constructor implementation
RiemannIntegral::RiemannIntegral(std::function<double(double)> f, double a, double b)
    : f(f), a(a), b(b) {}

// Compute the Riemann integral of the function up to the Nth term
double RiemannIntegral::compute(unsigned int N) {
  current_value = 0;  // Reset current_value
  double delta_x = (b - a) / (current_index + N);  // Compute the width of each rectangle in the Riemann sum
  for (unsigned int i = 1; i <= current_index + N; ++i) {
    double x_i = a + i * delta_x;  // Compute the x-coordinate of the right side of each rectangle
    current_value += f(x_i) * delta_x;  // Add the area of each rectangle to the total sum
  }
  current_index += N;  // Update current_index
  return current_value;  // Return the total sum (the Riemann integral)
}
