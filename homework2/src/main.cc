#include <iostream>
#include <memory>
#include <sstream>
#include <fstream>
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "PrintSeries.hh"
#include "WriteSeries.hh"
#include "RiemannIntegral.hh"


std::function<double(double)> set_function(char choice) {

  std::function<double(double)> fct;
  switch (choice) {
    case 'c':
      fct = [&](double x) { return cos(x); };
      break;
    case 's':
      fct = [&](double x) { return sin(x); };
      break;
    case 'x':
      fct = [&](double x) { return pow(x, 3); };
      break;
    default:
      throw std::invalid_argument( "Invalid function choice: " + std::string(1, choice) );
  }
  return fct;
}


int main(int argc, char* argv[]) {

  /* Parsing arguments */

  // Create a stringstream from the command-line arguments
  std::stringstream ss;
  for (int i = 1; i < argc; ++i) {
    ss << argv[i] << ' ';
  }

  char seriesChoice, functionChoice = '\0', outputChoice;
  double a = 0, b = 0;
  unsigned int N;

  switch (argc) {
    case 4:
      ss >> seriesChoice >> N >> outputChoice;
      break;

    case 7:
      ss >> seriesChoice >> N >> outputChoice >> functionChoice >> a >> b;
      break;

    default: {
      std::string message = "Invalid number of arguments (argc=" + std::to_string(argc) + "). Please provide either 3 or 6 arguments.";
      message += "\nUsage: " + std::string(argv[0])  + " seriesChoice N outputChoice [functionChoice a b]";
      throw std::invalid_argument(message);
    }
  }


  /* Series selection */

  std::unique_ptr<Series> series;

  switch (seriesChoice) {
    case 'a':
      series = std::make_unique<ComputeArithmetic>();
      break;

    case 'p':
      series = std::make_unique<ComputePi>();
      break;

    case 'i':
      series = std::make_unique<RiemannIntegral>(set_function(functionChoice), a, b);
      break;

    default:
      throw std::invalid_argument( "Invalid series choice: " + std::string(1, seriesChoice) );
  }

  /* Compute and dump Series */

  std::cout.precision(10); // Set the precision of std::cout
  switch (outputChoice) {
    case 'p': {
      PrintSeries printer(*series, N / 10, N);
      printer.setPrecision(10); // Set the precision of the printer
      printer.dump(std::cout); // Pass std::cout as the output stream
      break;
    }
    case 'w': {
      WriteSeries writer(*series, N / 10, N);
      writer.setSeparator(" "); // Set the separator to space
      writer.setPrecision(10); // Set the precision of the writer
      std::ofstream ofs(writer.getFilename()); // Open the file with the name set in WriteSeries
      writer.dump(ofs); // Pass the ofstream as the output stream
      break;
    }
    default:
      throw std::invalid_argument( "Invalid output choice: " + std::string(1, outputChoice) );
  }

  return 0;
}
