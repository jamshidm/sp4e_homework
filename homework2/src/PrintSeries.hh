#ifndef PRINTSERIES_HH
#define PRINTSERIES_HH

#include "DumperSeries.hh"
#include <iostream>

// Class for printing a series to the console, derived from DumperSeries
class PrintSeries : public DumperSeries {
public:
    // Constructor takes a Series object, a frequency, and a max iteration count
    PrintSeries(Series& series, unsigned int frequency, unsigned int maxiter);
    
    // Override the dump method to print to the console
    void dump(std::ostream& os = std::cout) override;
private:
    unsigned int frequency;  // The frequency of printing
    unsigned int maxiter;  // The maximum iteration count
};

#endif
