#include "PrintSeries.hh"
#include <cmath>

// Constructor implementation
PrintSeries::PrintSeries(Series& series, unsigned int frequency, unsigned int maxiter)
  : DumperSeries(series), frequency(frequency), maxiter(maxiter) {}

// Dump (print) the series to the console with a certain frequency up to maxiter iterations
void PrintSeries::dump(std::ostream& os) {
  double prediction = series.getAnalyticPrediction();  // Get the analytic prediction of the series
  os.precision(precision);  // Set the precision of the output stream
  for (unsigned int i = 0; i < maxiter; i += frequency) {
      double value = series.compute(frequency);  // Compute the value of the series up to the current iteration
      os << "Iteration " << i + frequency << ": " << value;  // Print the iteration number and value

      // If the analytic prediction is not NaN, print the residual
      if (!std::isnan(prediction)) {
          os << " (Residual: " << prediction - value << ")";
      }

      os << std::endl;
  }
}
