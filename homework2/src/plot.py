import matplotlib.pyplot as plt
import pandas as pd
from os import path

# Define the path to the file
file_path = path.join(path.dirname(path.dirname(path.abspath(__file__))), 'output')

# map file extension with separator
map_ext_sep = {'.csv': ',', '.psv': '|', '.txt': ' ', None: None}
for ext, sep in map_ext_sep.items():
    if ext is None:
        raise FileNotFoundError('Output file not found')

    file = file_path + ext
    if path.exists(file):
        data = pd.read_csv(file, sep=sep, header=None)
        break

# If data is not empty, plot the data
if not data.empty:
    plt.plot(data[0], data[1], 'o-', label='Numerical Result')

    # If there's a third column in the data, plot it as the analytic prediction
    if len(data.columns) > 2:
        plt.plot(data[0], data[2], label='Residual')

    plt.xlabel('Iteration')
    plt.ylabel('Series value')
    plt.legend()
    plt.show()
