#ifndef COMPUTEARITHMETIC_HH
#define COMPUTEARITHMETIC_HH

#include "Series.hh"

// Class for computing an arithmetic series, derived from Series
class ComputeArithmetic : public Series {
public:
    // Override the compute method for an arithmetic series
    double compute(unsigned int N) override;
};

#endif
