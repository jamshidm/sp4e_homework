#ifndef SERIES_HH
#define SERIES_HH

#include <cmath>

// Abstract base class for a series
class Series {
public:
    // Compute the Nth term of the series
    virtual double compute(unsigned int N) = 0;
    
    // Get the analytic prediction of the series, default is NaN
    virtual double getAnalyticPrediction() { return std::nan(""); }
protected:
    unsigned int current_index = 0;  // The current index of computation
    double current_value = 0;  // The current value of computation
};

#endif
