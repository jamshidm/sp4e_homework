#include "ComputePi.hh"
#include <cmath>

// Compute Pi using a specific series up to the Nth term
double ComputePi::compute(unsigned int N) {
    for (int i(current_index); i < current_index + N; ++i) {
        current_value += 1 / pow(i + 1, 2);
    }
    current_index += N;
    return sqrt(6 * current_value);
}

// Return the analytic prediction of Pi, which is M_PI in C++
double ComputePi::getAnalyticPrediction() {
    return M_PI;
}
