#ifndef RIEMANNINTEGRAL_HH
#define RIEMANNINTEGRAL_HH

#include "Series.hh"
#include <functional>

// Class for computing a Riemann integral of a function, derived from Series
class RiemannIntegral : public Series {
public:
    // Constructor takes a function and the limits of integration
    RiemannIntegral(std::function<double(double)> f, double a, double b);
    
    // Override the compute method to compute the Riemann integral
    double compute(unsigned int N) override;
private:
    std::function<double(double)> f;  // The function to be integrated
    double a;  // The lower limit of integration
    double b;  // The upper limit of integration
};

#endif
