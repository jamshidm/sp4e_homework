#include "WriteSeries.hh"
#include <iostream>
#include <cmath>

// Constructor implementation. Default filename is "output.txt" and default separator is "\t"
WriteSeries::WriteSeries(Series& series, unsigned int frequency, unsigned int maxiter)
  : DumperSeries(series), frequency(frequency), maxiter(maxiter), filename("../../output.txt"), separator("\t") {}

// Dump (write) the series to a file with a certain frequency up to maxiter iterations. Also writes residuals if available.
void WriteSeries::dump(std::ostream& os) {
  double prediction = series.getAnalyticPrediction();
  os.precision(precision);  // Set the precision of the output stream
  for (unsigned int i = 0; i < maxiter; i += frequency) {
    double value = series.compute(frequency);
    os << i + frequency << separator << value;

    if (!std::isnan(prediction)) {
      os << separator << prediction - value;
    }

    os << "\n";
  }
}

// Set the separator and update the filename accordingly. Throws an exception if an unknown separator is used.
void WriteSeries::setSeparator(const std::string& separator) {
  this->separator = separator;

  if (separator == ",") {
    filename = "../../output.csv";
  } else if (separator == " ") {
    filename = "../../output.txt";
  } else if (separator == "|") {
    filename = "../../output.psv";
  } else {
    throw std::invalid_argument("Unknown separator");
  }
}

// Set the precision of the output file stream
void WriteSeries::setPrecision(unsigned int precision) {
  this->precision = precision;
}

// Get the filename of the output file
std::string WriteSeries::getFilename() const {
  return filename;
}
