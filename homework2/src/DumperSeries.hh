#ifndef DUMPERSERIES_HH
#define DUMPERSERIES_HH

#include "Series.hh"
#include <ostream>

// Abstract base class for dumping a series to an output stream
class DumperSeries {
public:
    // Constructor takes a Series object
    DumperSeries(Series& series) : series(series) {}
    
    // Virtual method for dumping the series
    virtual void dump(std::ostream& os) = 0;
    
    // Method for setting the precision of the output, default is 6
    virtual void setPrecision(unsigned int precision) { this->precision = precision; }
    
    // Overload the << operator for easy output
    friend std::ostream& operator<<(std::ostream& os, DumperSeries& _this);
protected:
    Series& series;  // The series to be dumped
    unsigned int precision = 6;  // The precision of the output
};

// Implementation of the << operator overload
inline std::ostream& operator<<(std::ostream& os, DumperSeries& _this) {
    _this.dump(os);
    return os;
}

#endif
