#include "ComputeArithmetic.hh"
#include <iostream>

// Compute the sum of the first N natural numbers
double ComputeArithmetic::compute(unsigned int N) {
  for (int i(current_index); i < current_index + N; ++i) {
    current_value += i + 1;
  }
  current_index += N;
  return current_value;
}
