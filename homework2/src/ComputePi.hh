#ifndef COMPUTEPI_HH
#define COMPUTEPI_HH

#include "Series.hh"

// Class for computing Pi using a specific series, derived from Series
class ComputePi : public Series {
public:
    // Override the compute method to compute Pi
    double compute(unsigned int N) override;
    
    // Override the getAnalyticPrediction method to return the value of Pi
    double getAnalyticPrediction() override;
};

#endif
