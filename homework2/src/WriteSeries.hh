#ifndef WRITESERIES_HH
#define WRITESERIES_HH

#include "DumperSeries.hh"
#include <fstream>
#include <string>

// Class for writing a series to a file, derived from DumperSeries
class WriteSeries : public DumperSeries {
public:
    // Constructor takes a Series object, a frequency, and a max iteration count
    WriteSeries(Series& series, unsigned int frequency, unsigned int maxiter);
    
    // Override the dump method to write to a file
    void dump(std::ostream& os) override;
    
    // Method for setting the separator in the file and updating the filename accordingly
    void setSeparator(const std::string& separator);
    
    // Override the setPrecision method to update the precision of the output file stream
    void setPrecision(unsigned int precision) override;
    
    // Method for getting the filename of the output file
    std::string getFilename() const;
private:
    unsigned int frequency;  // The frequency of writing to the file
    unsigned int maxiter;  // The maximum iteration count for writing to the file
    std::ofstream file;  // The output file stream object
    std::string separator;  // The separator used in the file (e.g., comma, space)
    std::string filename;  // The name of the output file 
};

#endif
