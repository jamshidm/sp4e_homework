# SP4E Homework 2

_Mohammad Jamshidmofid & Loïc Jeanningros_

Directory for handing the second homework of _Scientific Programming for Engineers_ course

## Installation
First clone the repository and go to the created directory:
```commandline
git clone https://gitlab.epfl.ch/jamshidm/sp4e_homework.git
cd sp4e_homework/homework2
```

It is always a good idea to create a virtualenv and activate it:
```commandline
virtualenv venv
. venv/bin/activate
```
To install required libraries run:
```commandline
pip install -r requirements.txt
```

## Usage
To run the `main` executable developped for homework 2, run the following commands:
```commandline
cd build/src
cmake ..
make
./main a 100 p
./main i 200 w x 0 1
```
Arguments:

- The first argument selects a type of series. Options are:
  - **a** for ComputeArithmetic
  - **p** for ComputePi
  - **i** for RiemannIntegral (Integral)
- The second argument defines the number of iterations N
- The third argument selects the output modality. Options are:
  - **p** for printing on the screen
  - **w** for writing in a file

If you select RiemannIntegral (i.e., i), you need to provide three additional arguments:
- The fourth argument selects one of the following function to be integrated:
  - **c** for _cos(x)_
  - **s** for _sin(x)_
  - **x** for _x^3_
- The fifth and sixth arguments are for the lower (a) and upper (b) bounds of integration, respectively.

Examples:

- To compute an arithmetic series with 100 iterations and print the results on the screen, use: ./main a 100 p
- To compute a Riemann integral of x^3 from 0 to 1 with 200 iterations and write the results to a file, use: ./main i 200 w x 0 1

#### Python script

To run the python script that plot Series convergence, run the following command from `homework2` directory
```commandline
python3 src/plot.py
```

## Written answers
#### Exercise 2
In theory, the best way to divide the work is to attribute to each of us different exercises such that each one 
works in a different file. For instance, Mohammad starts with the Series classes and Loïc with the Dumper classes. Then,
 each one will communicate when starting a new exercise/file. We also try to commit / pull / push as often as necessary.

In practice, Loïc was in holidays. Then Mohammad started the exercises during the first days, and Loïc took over by 
reviewing and completing exercises during the last days.

#### Exercise 5
.1 - The complexity of the program is O(N<sup>2</sup>)

.4 - Now the complexity becomes O(N)

.5 - Summing terms reversely does not change the complexity which is still O(N)

#### Exercise 6
To get a presicion of 10<sup>-2</sup>:
- For the integral of _x<sup>3</sup>_ between _0_ and _1_, we need a bit more than 50 iterations
- For the integral of _cos_ between 0 and _pi_, we need around 350 iterations
- For the integral of _sin_ between 0 and _pi/2_, we need around 80 iterations
