import argparse
import numpy as np

from GMRES import gmres
from optimizer import optimize

from plot import plot_iterations


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Solve the system of linear equations Ax = b')
    parser.add_argument('-A', nargs='+', type=float,
                        help='Matrix A (as a list of squared length)')
    parser.add_argument('-b', nargs='+', type=float,
                        help='Vector b')
    parser.add_argument(
        '--method', type=str, default='GMRES',
        help='specify the method to solve Ax = b. '
             '`GMRES` is the one we developed ourselves, '
             '`LGMRES` uses scipy.sparse.linalg.lgmres and most methods from '
             'scipy.optimize.minimize are also available.')
    parser.add_argument(
        '--plot', action='store_true',
        help='will plot the solution and its convergence')
    args = parser.parse_args()

    args.A = np.array(args.A).flatten()
    if not np.sqrt(args.A.size).is_integer():
        raise ValueError('Size of input A must be a square number')
    n = int(np.sqrt(args.A.shape[0]))
    args.A = args.A.reshape((n, n))
    args.b = np.array(args.b).flatten()

    return args.A, args.b, args.method, args.plot


def S(x_, A_, b_):
    """Returns the value of the quadratic function at position x"""
    return 0.5 * x_.T @ A_ @ x_ - x_.T @ b_


if __name__ == '__main__':
    A, b, method, do_plot = parse_arguments()

    print(f'\nSolving Ax = b with {method} method ...\n')
    if method == 'GMRES':
        x_hist = gmres(A, b, tol=1e-3)
        S_hist = np.array([S(x_, A, b) for x_ in x_hist])
    else:
        x_hist, S_hist = optimize(S, A, b, method=method)

    print(f'According to the {method} method,\nthe solution of Ax = b is '
          f'x = ({x_hist[-1,0]:.3f}, {x_hist[-1,1]:.3f}),\n'
          f'which corresponds to S = {S_hist[-1]:.3f}\n')

    if do_plot:
        plot_iterations(S, x_hist, S_hist, method, A, b)
