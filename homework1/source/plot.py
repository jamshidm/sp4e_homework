import numpy as np
import matplotlib.pyplot as plt
import matplotlib

font = {'size': 12}
matplotlib.rc('font', **font)


def prepare_surface(S, A, b):
    # Create a grid of x values
    x1 = np.linspace(-1.5, 1.5, 100)
    x2 = np.linspace(-0.5, 2.5, 100)
    X1, X2 = np.meshgrid(x1, x2)

    # Calculate S(x) for each x in the grid
    Z = np.array([S(np.array([x1, x2]), A, b)
                  for x1, x2 in zip(np.ravel(X1), np.ravel(X2))])
    Z = Z.reshape(X1.shape)
    return X1, X2, Z


def plot_iterations(S, x_values: np.ndarray, S_values: np.ndarray, method: str,
                    A: np.ndarray, b: np.ndarray):
    """Plot the solution convergence on the surface of S(x) and the error for
    each iteration.
    """
    fig = plt.figure(figsize=(10, 10))
    fig.suptitle(f'Solving Ax = b with {method} method', fontsize=20)

    ax = fig.add_subplot(111, projection='3d')
    X1, X2, Z = prepare_surface(S, A, b)
    ax.contour3D(X1, X2, Z, 30, cmap='viridis')

    ax.plot(x_values[:, 0], x_values[:, 1], S_values,
            '-o', color='r', label='x')
    ax.view_init(20, 100)

    ax.set_xlabel(r'$x_1$', fontsize=20)
    ax.set_ylabel(r'$x_2$', fontsize=20)
    ax.set_zlabel('S(x)', fontsize=20)

    plt.legend()
    plt.show()
