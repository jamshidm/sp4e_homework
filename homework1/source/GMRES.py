import numpy as np


def gmres(
        A: np.ndarray,
        b: np.ndarray,
        x0: np.ndarray = None,
        max_iterations: int = 100,
        tol: float = 1e-5
) -> (np.ndarray, np.ndarray):
    """Generalized minimal residual method (GMRES)

    :arg A: squared invertible matrix
    :arg b: normed vector
    :arg x0: initial conditions of solution x
    :arg max_iterations: max number of iterations
    :arg tol: tolerance threshold

    :return: history of x positions
    """
    A = np.asarray(A, dtype=float)
    b = np.asarray(b, dtype=float)
    b = b.flatten()
    n = A.shape[0]
    m = max_iterations

    if A.ndim != 2:
        raise ValueError("matrix A must be 2-dimensional")
    if A.shape[1] != n:
        raise ValueError("matrix A must be a squared matrix")
    if b.size != n:
        raise ValueError("vector b must be the same size than A rows")
    if np.isclose(np.linalg.det(A), 0):
        raise ValueError('Singular matrix A')

    if x0 is None:
        x0 = np.array([1., 1.])
    else:
        x0 = np.asarray(x0, dtype=float)

    r = b - np.einsum('ij,j->i', A, x0)
    b_norm = np.sqrt(np.einsum('i,i', b, b))
    r_norm = np.sqrt(np.einsum('i,i', r, r))
    errors = [r_norm / b_norm]

    e1 = np.zeros((m + 1,))
    e1[0] = 1
    beta = r_norm * e1

    sn = np.zeros((m,))
    cs = np.zeros((m,))

    H = np.zeros((m + 1, m))
    Q = np.zeros((n, m + 1))
    Q[:, 0] = r / r_norm
    x_hist = [x0]
    for k in range(m):
        Q[:, k+1], H[:k+2, k] = arnoldi_step(A, Q, k)
        H[:k+2, k], cs[k], sn[k] = apply_givens_rotation(H[:k+2, k], cs, sn, k)

        beta[k+1] = -sn[k] * beta[k]
        beta[k] = cs[k] * beta[k]

        error = np.abs(beta[k + 1] / b_norm)
        errors.append(error)

        try:
            y = np.linalg.solve(H[:k+1, :k+1], beta[:k+1])
            x_hist.append(x0 + np.einsum('ij,j', Q[:, :k + 1], y))
        except np.linalg.LinAlgError as err:
            print(err)
            break

        if error < tol:
            break

    return np.vstack(x_hist)


def arnoldi_step(A: np.ndarray, Q: np.ndarray, k: int):
    q = np.einsum('ij,j', A, Q[:, k])
    h = np.zeros((k+2,))
    for i in range(k+1):
        h[i] = np.einsum('i,i', q.conj(), Q[:, i])
        q -= h[i] * Q[:, i]
    h[k+1] = np.einsum('i,i', q, q)
    if h[k+1] > 1e-12:
        q /= h[k+1]
    else:
        return q, h
    return q, h


def apply_givens_rotation(h, cs, sn, k):
    for i in range(k):
        tmp = cs[i] * h[i] + sn[i] * h[i+1]
        h[i+1] = -sn[i] * h[i] + cs[i] * h[i+1]
        h[i] = tmp
    cs_k, sn_k = givens_rotation(h[k], h[k+1])
    h[k] = cs_k * h[k] + sn_k * h[k+1]
    h[k+1] = 0.
    return h, cs_k, sn_k


def givens_rotation(h1, h2):
    if h1 == 0:
        cs, sn = (0, 1)
    else:
        t = np.sqrt(h1**2 + h2**2)
        cs = h1 / t
        sn = h2 / t
    return cs, sn
