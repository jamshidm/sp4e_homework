# Import necessary libraries
import numpy as np
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres
from itertools import repeat


def optimize(S, A, b, x0=None, method='LGMRES'):
    """Solve the system of linear equations Ax = b

    :arg S: Surface function
    :arg A: matrix A
    :arg b: vector b
    :arg x0: initial conditions
    :arg method: method to use to solve the problem. Can be `LGMRES` or any
                method accepted by scipy.optimize.minimize (`BFGC`, `CG`, ...)
    """
    if x0 is None:
        x0 = np.zeros((2,))
    x_values = [x0]

    if method == 'LGMRES':
        x_min, _ = lgmres(
            A, b, x0=x0, atol=1e-5,
            callback=lambda x: x_values.append(x),
        )
    else:
        _ = minimize(
            lambda x: S(x, A, b), x0, method=method,
            callback=lambda x: x_values.append(x),
        )

    S_values = list(map(S, x_values, repeat(A), repeat(b)))

    # print(f"\nAccording to the {method} method, "
    #       f"the minimum value of S(x) is achieved at {x_min}.")
    # print(f"This minimum value of S(x) is {S_values[-1]}.\n")

    return np.vstack(x_values), np.hstack(S_values)


if __name__ == '__main__':
    A_ = np.array([[8, 1], [1, 3]])
    b_ = np.array([2, 4])
    for method_ in ['BFGS', 'LGMRES']:
        x_vales = optimize(A_, b_, method_)
