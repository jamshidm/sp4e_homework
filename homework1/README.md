# SP4E Homework

_Mohammad Jamshidmofid & Loïc Jeanningros_

Python Package for handing in _Scientific Programming for Engineers_ course assignments

## Installation
The `sp4e_homework` package can be easily installed using `pip`.
First clone the repository and go to the created directory:
```commandline
git clone https://gitlab.epfl.ch/jamshidm/sp4e_homework.git
cd sp4e_homework
```

It is always a good idea to create a virtualenv and activate it:
```commandline
virtualenv venv
. venv/bin/activate
```
To install required libraries run:
```commandline
pip install -r requirements.txt
```

## Usage
To run the program developped during week 4 (first homework), run the following command line:
```commandline
python3 source/week4.py -A 8 1 1 3 -b 2 4 --method GMRES --plot
```
It will solve the linear system of equations **A**_x_ = _b_ with the method we wrote ourselves.

arguments:

`-A` matrix A written as a flat array. In the example A = [[8, 1], [1, 3]]

`-b` vector b. In the example b = [2, 4]

options:

`--method` choose the method used to solve the problem  (GMRES, LGMRES, BFGS, CG, ...)
GMRES: Generalized Minimum RESidual
LGMRES: Limited-memory Generalized Minimum RESidual
BFGS: Broyden–Fletcher–Goldfarb–Shanno
CG: Conjugate Gradient

`--plot` plot the solution and its convergence over the surface of the optimization problem

More information available -> `python3 source/week4.py --help`

## Note

Unfortunately, the GMRES method we implemented is very unstable and often diverge. 
Choosing other inputs **A** and **b** or different initial conditions **x0** will probably 
lead to a non-optimal solution.
