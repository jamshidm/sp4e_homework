import numpy as np
import sys

def compute_frequencies(size):
    
    freqs = np.fft.fftfreq(size)
    freqs_matrix = np.array(np.meshgrid(freqs, freqs)).T.reshape(-1, 2)
    complex_freqs_matrix = freqs_matrix[:, 0] + 1j * freqs_matrix[:, 1]
    complex_freqs_matrix = complex_freqs_matrix.reshape((size, size))
    return complex_freqs_matrix
    
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python test_freq.py size")
        sys.exit(1)
    
    size = int(sys.argv[1])
    freq_matrix = compute_frequencies(size)
    print(freq_matrix)
