#include "compute_temperature.hh"
#include "material_points_factory.hh"
#include <gtest/gtest.h>
#include <cmath>

std::vector<Real> getSystemState(System& system) {
  std::vector<Real> state = std::vector<Real>(system.getNbParticles());
  for (UInt i = 0; i != system.getNbParticles(); ++i) {
    auto par = dynamic_cast<MaterialPoint&>(system.getParticle(i));
    state[i] = par.getTemperature();
  }
  return state;
}

std::vector<Real> getAnalyticalSolution(System& system) {
  std::vector<Real> solution = std::vector<Real>(system.getNbParticles());
  for (UInt i = 0; i != system.getNbParticles(); ++i) {
    auto par = dynamic_cast<MaterialPoint&>(system.getParticle(i));
    Vector position = par.getPosition();
    solution[i] = sin(M_PI * position[0]);
  }
  return solution;
}

TEST(compute_temperature, no_heat) {
  std::string filename = "../inputs/inputs_noheat.csv";
  Real timestep = 5e-3;
  UInt nsteps = 10;
  UInt freq = 10;
  MaterialPointsFactory::getInstance();
  ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
  SystemEvolution& evol = factory.createSimulation(filename, timestep);

  std::vector<Real> initial_state = getSystemState(evol.getSystem());
  evol.setNSteps(nsteps);
  evol.setDumpFreq(freq);
  evol.evolve();
  std::vector<Real> final_state = getSystemState(evol.getSystem());

  for (UInt i = 0; i < initial_state.size(); ++i) {
    ASSERT_NEAR(final_state[i], initial_state[i], 1e-10);
  }
}

TEST(compute_temperature, with_heat) {
  std::string filename = "../inputs/inputs_heat.csv";
  Real timestep = 5e-3;
  UInt nsteps = 1001;
  UInt freq = 100;
  MaterialPointsFactory::getInstance();
  ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
  SystemEvolution& evol = factory.createSimulation(filename, timestep);

  evol.setNSteps(nsteps);
  evol.setDumpFreq(freq);
  evol.evolve();
  std::vector<Real> final_state = getSystemState(evol.getSystem());

  std::vector<double> analytical_sol = getAnalyticalSolution(evol.getSystem());
  for (UInt i = 0; i <= final_state.size(); ++i) {
    ASSERT_NEAR(final_state[i], analytical_sol[i], 1e-2);
  }
}
