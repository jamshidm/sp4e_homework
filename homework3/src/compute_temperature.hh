#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

public:
  ComputeTemperature(Real dt);

public:
  void compute(System& system) override;

private:
  Real dt; // timestep
  Real kappa = 1.0; // heat conductivity
  Real rho = 1.0; // mass density
  Real C = 1.0; // specific heat capacity

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
