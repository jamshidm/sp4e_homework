import numpy as np

import argparse

parser = argparse.ArgumentParser(
    description="Generate initial state for a system of material points")
parser.add_argument("number", help="number of particles", type=int)  # 262144
parser.add_argument("filename", help="name of generated input file")
parser.add_argument("--heat-source", action="store_true")

args = parser.parse_args()

n_particles = args.number
size = int(np.sqrt(n_particles))
if size * size != n_particles:
    raise ValueError("number must be a squared number")

w = np.linspace(-1, 1, size, endpoint=False)
z = np.zeros((n_particles,))
x = np.repeat(w, size)
y = np.tile(w, size)

positions = np.vstack((x, y, z))
velocity = np.vstack((z, z, z))
force = np.vstack((z, z, z))
mass = z

heat_rates = np.zeros((n_particles,))
heat_sources = np.zeros((n_particles,))
if args.heat_source:
    for i, xx in enumerate(x):
        heat_sources[i] = np.pi * np.pi * np.sin(np.pi * xx)
    temperatures = np.zeros((n_particles,))
else:
    temperatures = np.ones((n_particles,))

file_data = np.vstack(
    (positions, velocity, force, mass, temperatures, heat_rates, heat_sources)).T
np.savetxt(args.filename, file_data, delimiter=" ")
