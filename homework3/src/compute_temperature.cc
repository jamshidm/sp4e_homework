#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include "matrix.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

ComputeTemperature::ComputeTemperature(Real dt) : dt(dt) {}

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {
  // Create the matrix
  auto n_par = static_cast<double>(system.getNbParticles());
  auto size = static_cast<UInt>(sqrt(n_par));
  Matrix<complex> T(size);
  Matrix<complex> H(size);

  for (auto&& entry: index(T)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(i*size + j));

    val = static_cast<complex>(par.getTemperature());
    H(i, j) = static_cast<complex>(par.getHeatSource());
  }

  // apply Fourier Transform
  Matrix<complex> T_hat = FFT::transform(T); // T for Temperature
  Matrix<complex> H_hat = FFT::transform(H); // H for Heat rate
  Matrix<complex> dT_hat(size);
  auto frequencies = FFT::computeFrequencies(size);

// apply solver
  for (auto&& entry : index(T_hat)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    auto f = frequencies(i, j);
    double qx = f.real();
    double qy = f.imag();
    double k_sq = (qx * qx + qy * qy); // * M_PI * M_PI;
    dT_hat(i, j) = (H_hat(i, j) - kappa * val * k_sq) / (rho * C);
  }

  // apply inverse Fourier Transform
  Matrix<complex> Y = FFT::itransform(dT_hat);

  // update temperature directly into the system
  for (auto&& entry : index(Y)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    auto& par = dynamic_cast<MaterialPoint&>(system.getParticle(i*size + j));
    par.getTemperature() += dt * val.real();
    par.getHeatRate() = val.real();
  }
}
/* -------------------------------------------------------------------------- */