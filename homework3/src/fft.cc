#include "fft.hh"
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
#include <cmath>

Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  UInt N = m_in.size();
  auto *m_in_fftw = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N);
  auto *m_out_fftw = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N);
  fftw_plan p = fftw_plan_dft_2d(
      N, N, m_in_fftw, m_out_fftw,FFTW_FORWARD, FFTW_ESTIMATE);
  Matrix<complex> m_out(N);

  for (auto&& entry: index(m_in)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    m_in_fftw[i*N + j][0] = val.real();
  }

  fftw_execute(p);

  for (auto&& entry: index(m_out)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = std::complex<double>(m_out_fftw[i*N + j][0],m_out_fftw[i*N + j][1]);
  }

  fftw_destroy_plan(p);
  fftw_free(m_in_fftw);
  fftw_free(m_out_fftw);
  fftw_cleanup();

  return m_out;
}

Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  UInt N = m_in.size();
  fftw_complex *m_in_fftw = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N);
  fftw_complex *m_out_fftw = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N);
  fftw_plan p = fftw_plan_dft_2d(N, N, m_in_fftw, m_out_fftw, FFTW_BACKWARD, FFTW_ESTIMATE);
  Matrix<complex> m_out(N);

  for (auto&& entry: index(m_in)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    m_in_fftw[j + N * i][0] = val.real();
    m_in_fftw[j + N * i][1] = val.imag();
  }

  fftw_execute(p);

  for (auto&& entry: index(m_out)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = std::complex<double>(m_out_fftw[j + N * i][0] / (N * N), 0.);
  }

  fftw_destroy_plan(p);
  fftw_free(m_in_fftw);
  fftw_free(m_out_fftw);
  fftw_cleanup();

  return m_out;
}

Matrix<std::complex<int>> FFT::computeFrequencies(int size) {

  int k[size];
  Matrix<std::complex<int>> freq(size);

  for(UInt i = 0; i < size; ++i) {
      if(i < size/2) { k[i] = i; }
      else { k[i] = i-size; }
  }

  for (UInt i = 0; i < size; ++i){
      for (UInt j = 0; j < size; ++j){
        freq(i,j) =std::complex<int>(k[i], k[j]);
    }
  }

  return freq;
}