# SP4E Homework 3

_Mohammad Jamshidmofid & Loïc Jeanningros_

Directory for handing the third homework of _Scientific Programming for Engineers_ course

## Installation
First clone the repository and go to the created directory:
```commandline
git clone https://gitlab.epfl.ch/jamshidm/sp4e_homework.git
cd sp4e_homework/homework3
```

Second, initialize submodules:
```commandline
git submodule update --init
```

For Python use, it is always a good idea to create a virtualenv and activate it:
```commandline
virtualenv venv
. venv/bin/activate
```
To install required libraries run:
```commandline
pip install -r requirements.txt
```

## Usage
To run the `particles` executable developed for homework 3, run the following commands:
```commandline
cd homework3
git submodule add https://github.com/google/googletest.git
mkdir build
cd build
cmake -DUSE_FFTW=ON .. (or OFF if FFTW is not going to be used)
make
src/particles 1001 100 ../inputs/inputs_heat.csv material_point 1e-5
```
To run the tests, after compiling:
```commandline
./src/test_fft
./src/test_temperature
./src/test_kepler
python3 ../src/test_freq.py 512
```

## Exercise 1
`MaterialPoint` is a particle object itself. It does not represent a real object like Planets or PingPongBalls 
but is rather a point in a lattice (mesh) used for numerical modeling. `MaterialPoint` has three members: 
`temperature` a state variable, `heat_rate` which is the derivative of the temperature and `heat_source` that provide 
the shape of the heat source.

`Matrix` is a container, made to handle 2D nets. It is actually implemented as a 1D array but provides methods to use it 
as a 2D matrix. It also provides useful functions to iterate through the `Matrix`.

`MaterialPointFactory` is a basic Factory with the particularity that the number of particles must be squared to create 
the lattice of `MaterialPoints`.

`FFT` is a made of a `Matrix` of complex numbers. It is not  directly linked to any `Particle`, but it offers very 
convenient methods to compute spectral transformations.

## Exercise 4
After running the `particles` executable for a material_points simulation (see an example in Usage section), states of the system are regularly saved in
 the `dumps` folder.

Once the .csv file from the simulation is created, the following steps can be taken to visualize the data with Paraview:

`Import the .csv File:` Navigate to the ‘File’ menu and select ‘Open’. Locate the .csv file in the ‘dumps’ folder and open it.

`Configure the Import Settings:` In the ‘Data Source’ settings, set the field Delimiter Character to a space.

`Convert Data to Points:` Apply the ‘Table To Points’ filter to convert the data points into coordinates. Set the x-coordinate as field 0, y-coordinate as field 1, z-coordinate as field 2.

`Apply a Filter:` Apply a suitable filter to the object created from the ‘Table to points’ filter.

`Visualize the Temperature Field:` The temperature field can be visualized by setting the ‘Coloring’ option to the .csv temperature field.

## Comments

The implemented solution of the heat equation with a heat source does not converge to the expected solution, 
and besides, the test 'test_temperature' fails. 